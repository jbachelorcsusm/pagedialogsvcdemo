﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Prism.Services;
using PageDialogSvcDemo.Helpers;

namespace PageDialogSvcDemo.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        INavigationService _navigationService;
        IPageDialogService _pageDialogService;

        public DelegateCommand ShowAlertCommand { get; set; }
        public DelegateCommand ShowActionSheetCommand { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MainPageViewModel)}:  ctor");

            Title = "Main Page";
            _navigationService = navigationService;
            _pageDialogService = pageDialogService;

            ShowAlertCommand = new DelegateCommand(OnShowAlert);
            ShowActionSheetCommand = new DelegateCommand(OnShowActionSheet);
        }

        private async void OnShowActionSheet()
        {
            string userResponse = await _pageDialogService.DisplayActionSheetAsync(Constants.SHOW_ACTION_TITLE,
                                                                                   Constants.SHOW_ACTION_CANCEL,
                                                                                   Constants.SHOW_ACTION_DESTROY,
                                                                                   Constants.SHOW_ACTION_YES,
                                                                                   Constants.SHOW_ACTION_NO);
            
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnShowActionSheet)}:  User response:  {userResponse}");
        }

        private async void OnShowAlert()
        {
            bool userResponse = await _pageDialogService.DisplayAlertAsync(Constants.RED_ALERT_TITLE, 
                                                                     Constants.RED_ALERT_MESSAGE, 
                                                                     Constants.RED_ALERT_ACCEPT, 
                                                                     Constants.RED_ALERT_CANCEL);
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnShowAlert)}:  User response: {userResponse}");
        }

        #region INavigationAware

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }

        #endregion INavigationAware
    }
}

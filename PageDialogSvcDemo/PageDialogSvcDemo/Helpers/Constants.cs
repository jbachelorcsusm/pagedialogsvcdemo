﻿using System;
namespace PageDialogSvcDemo.Helpers
{
    public static class Constants
    {
        public const string RED_ALERT_TITLE = "Red Alert!";
        public const string RED_ALERT_MESSAGE = "Do you really want to fire off a serious red alert?";
        public const string RED_ALERT_ACCEPT = "You Bet!";
        public const string RED_ALERT_CANCEL = "No Way";
        public const string SHOW_ACTION_TITLE = "Bring The Action?";
        public const string SHOW_ACTION_CANCEL = "Cancel";
        public const string SHOW_ACTION_NO = "Heck No";
        public const string SHOW_ACTION_DESTROY = "Destroy Everything";
        public const string SHOW_ACTION_YES = "Yes Please!";
    }
}

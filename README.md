# PageDialogService Exercise

This exercise is designed to show you how to implement alerts and action sheets on iOS and Android using Xamarin.Forms and the Prism framework's __PageDialogService__.

## Exercise Steps
1. Start by downloading the solution from the [ExerciseStart tag](https://bitbucket.org/jbachelorcsusm/pagedialogsvcdemo/get/ExerciseStart.zip), and opening it in Visual Studio.
1. Let's create our delegate commands first. Make two:  *ShowAlertCommand* and *ShowActionSheetCommand*. 
    1. Add the command bindings to the buttons in your view.
![Command binding example](CommandBindings.png)
    1. Declare the commands in the MainPageViewModel.
![Declare commands](DeclareCommands.png)
    1. New them up in the ViewModel __constructor__.
![Instantiate commands](NewUpCommandsWithHandlers.png)
    1. Generate methods (by hitting *alt-enter* while your cursor is on each handler name) to implement the commands. Just add debug statements to each command implementation for now. Note:  Your handlsers will not be *async* by default, but notice that is how the screenshot below is. I've added the async keyword as we will soon be needing to await a user response. Remember:  A handler is the one and only place where having an *async void* method is acceptable. Anywhere else, it should be *async Task*.
![Command handlers](CreateHandlerStub.png)
1. Our ViewModel already has requested an *INavigationService*. In order to display alerts and action sheets, we will need to request Prism's *IPageDialogService*. This is all part of *dependency injection*. Prism provides the PageDialogService, just like it provides the NavigationService. You can get an instance of the PageDialogService by adding an IPageDialogService to your ViewModel constructor paramter list:
![Constructor Example, including IPageDialogService](VMCtorWithIPDS.png)
1. Just like we store the instance of the NavigationService in a field called *_navigationService*, you should add a field to store the PageDialogService (maybe name it something like *_pageDialogService*).
    * Now your MainPageViewModel is armed with an IPageDialogService. We're ready to rock!
![Assignment of field in constructor](ConstructorWithIPDS.png)
1. Implement the display of an alert in the implementation method for the *ShowAlertCommand*. Print a debug line out showing what the user chose:  Ok or Cancel (true or false).
    * Hint: You'll have to make your command handler async, if you haven't already.
![DisplayAlert implementation](DisplayAlert.png)
1. Run the app, and try out your alert. Make sure it is working properly.
1. In the *Helpers* folder, there is a *Constants.cs* static class. Add constants for the options you plan to provide in your action sheet. Add at least four options for the user:
    * Yes please!
    * Heck no
    * Cancel (this should be the cancel option, displayed at the very bottom)
    * Destroy Everything (this should be the destructive option, that is in red)
    * Here are some example constants I used for the alert above:
![Constants Example](Constants.png)
1. Implement the display of an action sheet in the implementation method for the ShowActionSheetCommand. Use the constants you created for the text of each action, so there is no chance of a typo. Print a debug line out showing what option the user chose.
![DisplayActionSheet implementation](DisplayTheAction.png)
1. Bask in your alerty, actiony glory... You're an IPageDialogService pro!

![Victory  image](VictoryIsMine.jpg)